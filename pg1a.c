/*
Jeffrey Yu
Assignment 1b
600.120
408 499 1338
jyu42
c.jeffyu@gmail.com

This assignment was difficult at the start, as with most new programming assignments. Once I better understood the bit operators things began to click. Figuring out spacing issues with the getChar() fuction was interesting as well. Also, error checking was tough, and I'm not sure if I covered everything.
*/

#include <stdio.h>
#include <ctype.h>
#include <string.h>

int main(void) {
	char ch;
	unsigned int ints[3];
	//Take in three integers
	printf("Please enter three integers between 0 and 2^32-1 (4,294,967,295) and press enter:\n");
	int i = 0, num;
	do {
		num = 0;
		ch = getchar(); //Get the next character
		while (!isspace(ch)) { //If the ch is not a space, add it to num; otherwise just reloop
			num = num * 10 + ch - '0';
			ch = getchar();
			if (!isdigit(ch) && !isspace(ch)) {
				printf("Bad input, quitting program.\n");
				return 0;
			}
		}
		if (isspace(ch)) { //If the character after a series of numbers is a space, store the number into the integer array
			ints[i] = num;
			i++;
			//printf("Integer %u is %u\n", i, num);
		}
	}
	while (i < 3);

	//Print out the RGB codes for each integer
	int rval[3], gval[3], bval[3];
	for (int i = 0; i < 3; i++) {
		rval[i] = (ints[i] & 0x00ff0000) >> 16;;
		gval[i] = (ints[i] & 0x0000ff00) >> 8;
		bval[i] = (ints[i] & 0x000000ff);
		printf("The RGB color code for the integer %10u is (%3d, %3d, %3d) (hex: %08x)\n", ints[i], rval[i], gval[i], bval[i], ints[i]);
	}
	//Create the combination color
	int combo;
	combo = (ints[0] & 0x00ff0000) + (ints[1] & 0x0000ff00) + (ints[2] & 0x000000ff);
	printf("The combination color code integer is %u, the RGB code is (%3d, %3d, %3d), and the hex value is #%06x\n", combo, rval[0], gval[1], bval[2], combo);
}
