/*
 Jeffrey Yu
 Assignment 1b
 600.120
 408 499 1338
 jyu42
 c.jeffyu@gmail.com
 Once the first program was written, this one was quite a bit simpler. At first, I just rewrote the printf lines to print to html and then added a loop to loop around. Unfortunately, I encountered some problems with the getchar() function in the while loop running first so the program would skip the first character entered. I couldn't figure out how to solve this problem, so I just added special instructions in the prompt. I also couldn't figure out how to get the prompt to show when I was writing to html. This program was tricky because of the aforementioned things.
 */

#include <stdio.h>
#include <ctype.h>
#include <string.h>

int main(void) {
	printf("Please enter sets of three integers between 0 and 2^32-1.\nTo start a new line, press return twice.\nTo exit, press enter once, then ^D.\n");
	printf("<html>\n<table>\n");
	char ch;
	do {
		int i = 0, num;
		unsigned int ints[3];
		printf("<tr>\n");
		do {
			num = 0;
			ch = getchar(); //Get the next character
			while (!isspace(ch)) { //If the ch is not a space, add it to num; otherwise just reloop
				num = num * 10 + ch - '0';
				ch = getchar();
				if (!isdigit(ch) && !isspace(ch)) {
					printf("Bad input, quitting program.\n");
					return 0;
				}
			}
			if (isspace(ch)) { //If the character after a series of numbers is a space, store the number into the integer array
				ints[i] = num;
				i++;
				//printf("Integer %u is %u\n", i, num);
			}
		} while (i < 3);
		//Print to html file the table with the colors and codes
		for (int i = 0; i < 3; i++) {
			printf("<td bgcolor=\"#%06x\"> <font color=\"%06x\">#%06x</font></td>\n", (ints[i] & 0xffffff), 0xffffff - (ints[i] & 0xffffff), (ints[i] & 0xffffff));
		}
		//Create the combination color
		int combo;
		combo = (ints[0] & 0x00ff0000) + (ints[1] & 0x0000ff00) + (ints[2] & 0x000000ff);
		printf("<td bgcolor=\"#%06x\"> <font color=\"%06x\">#%06x</font></td>\n", combo, 0xffffff - combo, combo);
		printf("</tr>\n");
	}
	while ((ch = getchar()) != EOF);
	printf("</table>\n</html>\n");
}
